package com.example.meuprimeiroapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val campo1 = findViewById<EditText>(R.id.num1)
        val campo2 = findViewById<EditText>(R.id.num2)
        val soma = findViewById<Button>(R.id.somar)
        val subt = findViewById<Button>(R.id.subtrair)
        val mult = findViewById<Button>(R.id.multiplicar)
        val divi = findViewById<Button>(R.id.dividir)
        soma.setOnClickListener {
            val input1 = campo1.text.toString()
            val input2 = campo2.text.toString()
            val soma = input1.toInt() + input2.toInt()
            val result = soma.toString()
            Toast.makeText(this,result,Toast.LENGTH_SHORT).show()
        }
        subt.setOnClickListener {
            val input1 = campo1.text.toString()
            val input2 = campo2.text.toString()
            val subt = input1.toInt() - input2.toInt()
            val result = subt.toString()
            Toast.makeText(this,result,Toast.LENGTH_SHORT).show()
        }
        mult.setOnClickListener {
            val input1 = campo1.text.toString()
            val input2 = campo2.text.toString()
            val mult = input1.toInt() * input2.toInt()
            val result = mult.toString()
            Toast.makeText(this,result,Toast.LENGTH_SHORT).show()
        }
        divi.setOnClickListener {
            val input1 = campo1.text.toString()
            val input2 = campo2.text.toString()
            val divi = input1.toInt() / input2.toInt()
            val result = divi.toString()
            Toast.makeText(this,result,Toast.LENGTH_SHORT).show()
        }

    }
}
